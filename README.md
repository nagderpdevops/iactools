# IaC Tools
The most reliable way to automate creating, updating, and deleting your cloud resources is to describe the target state of your infrastructure and use a tool to apply it to the current state of your infrastructure. AWS CloudFormation and Terraform are the most valuable tools to implement Infrastructure as Code on AWS.

Both tools are following a very similar approach.

1. You define a template (CloudFormation) or configuration (Terraform) describing the target state of your infrastructure.
2. The tool (CloudFormation or Terraform) calculates the necessary steps to reach the defined target.
3. The tool (CloudFormation or Terraform) executes the changes.

Here is the quick comparison tabel for the same.

![Table](comparison_table.png)