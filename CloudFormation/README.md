# AWS CloudFormation
It is an AWS specific service that allows to define a deployment of AWS resources in to “Templates”, that once provided to the CloudFormation service will create “Stacks” that can be managed. Once these “Stacks” are deployed, it becomes far easier to manage the resources “Stack”-wise to (say) rollout application updates or manage resource limitations.

The resources and their metadata are defined in JSON or YAML files. There is little learning curve to get started because of this. AWS also provides a visual diagramming tool, that could improve a lot at the moment this is being written.

![GUI](https://miro.medium.com/max/1050/1*tcK98tjpgcxjNwj5kAoJXg.png)

## Create Resource

Resources section is where the required AWS resources are defined, in any order. For example, if there is a need for an EC2 instance, an Elastic Load Balancer, and a Security Group to manage access, those can be defined in the Resources section as follows.

sample-resource.json

```
{
  "Resources": {
    "SampleEC2Instance": {
      "Type": "AWS::EC2::Instance",
      "Properties": {
        "ImageId": "ami-79fd7eee",
        "KeyName": "testkey",
        "SecurityGroup": {
          "Ref": "SampleSecurityGroup"
        },
        "BlockDeviceMappings": [{
            "DeviceName": "/dev/sdm",
            "Ebs": {
              "VolumeType": "io1",
              "Iops": "200",
              "DeleteOnTermination": "false",
              "VolumeSize": "20"
            }
          },
          {
            "DeviceName": "/dev/sdk",
            "NoDevice": {}
          }
        ]
      }
    },
    "SampleSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "Common Security Group",
        "VpcId": "SomeVPCID",
        "SecurityGroupEgress": [{
          "IpProtocol": -1,
          "FromPort": "0",
          "ToPort": "0",
          "CidrIp": "0.0.0.0/0"
        }],
        "SecurityGroupIngress": [{
            "IpProtocol": "tcp",
            "FromPort": "22",
            "ToPort": "22",
            "CidrIp": "0.0.0.0/0"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "443",
            "ToPort": "443",
            "CidrIp": "0.0.0.0/0"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "80",
            "ToPort": "80",
            "CidrIp": "0.0.0.0/0"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "3306",
            "ToPort": "3306",
            "CidrIp": "0.0.0.0/0"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "8080",
            "ToPort": "8080",
            "CidrIp": "0.0.0.0/0"
          },
          {
            "IpProtocol": "ICMP",
            "FromPort": "-1",
            "ToPort": "-1",
            "CidrIp": "0.0.0.0/0"
          }
        ]
      }
    },
    "SampleLoadBalancer": {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "CrossZone": true,
        "SecurityGroups": [{
          "Ref": "SampleSecurityGroup"
        }],
        "Subnets": [{
            "Ref": "PublicSubnet2"
          },
          {
            "Ref": "PublicSubnet1"
          }
        ],
        "LBCookieStickinessPolicy": [{
          "PolicyName": "LBStickyPolicy"
        }],
        "Instances": [{
          "Ref": "SampleEC2Instance"
        }],
        "Listeners": [{
            "LoadBalancerPort": "443",
            "InstancePort": "443",
            "Protocol": "HTTPS",
            "InstanceProtocol": "HTTPS",
            "PolicyNames": [
              "LBStickyPolicy"
            ],
            "SSLCertificateId": {
              "Fn::Join": [
                "", [
                  "arn:aws:iam::",
                  {
                    "Ref": "AWS::AccountId"
                  },
                  ":server-certificate",
                  "/",
                  {
                    "Ref": "CertificateName"
                  }
                ]
              ]
            }
          },
          {
            "LoadBalancerPort": "80",
            "InstancePort": "80",
            "Protocol": "HTTP",
            "InstanceProtocol": "HTTP",
            "PolicyNames": [
              "LBStickyPolicy"
            ]
          },
          {
            "LoadBalancerPort": "8080",
            "InstancePort": "8080",
            "Protocol": "TCP",
            "InstanceProtocol": "TCP"
          }
        ],
        "HealthCheck": {
          "Target": "TCP:80",
          "HealthyThreshold": "3",
          "UnhealthyThreshold": "5",
          "Interval": "10",
          "Timeout": "5"
        }
      }
    }
  }
}
```
