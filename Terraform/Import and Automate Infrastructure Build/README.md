# Import, Build and Deploy AWS Infrastructure using Terraform

The process of achieving this can be breakdown into three phases.
1. Importing existing infrastructure into teraform script
2. Adding state management for terraform
3. Creating _jenkins_ pipeline with aws infrastructure building tf script.

 ## 1. Importing infrastructure

To import from **AWS**, prior to running `terraform import` it is necessary to write manually a `resource` configuration block for the resource, to which the imported object will be mapped. The current implementation of Terraform import can only import resources into the state.

## Sample case Terraform Import Existing Resources AWS

Considering that our Infrastructure consists of these resources-
- aws_vpc
- aws_subnet
- aws_internet_gateway
- aws_eip
- aws_nat_gateway
- aws_route_table
- aws_route_table_association
- aws_security_group
- aws_instance
- aws_route53_zone
- aws_route53_record

### _Steps_

### Initialize Terraform
```
$ terraform init
```

### Add VPC Resource and Run Terraform Plan
```
$ terraform plan
```

### Import AWS VPC using vpc id
```
$ terraform import aws_vpc.main vpc-<id>
```

### Add Subnets to Terraform

### Import Subnets using ids
```
$ terraform import aws_subnet.public subnet-<id>
$ terraform import aws_subnet.private subnet-<id>
```

### Add IGW to Terraform

### Import IGW using id
```
$ terraform import aws_internet_gateway.igw igw-<id>
```

### Add EIP to Terraform

### Import EIP using Public IP
```
$ terraform import aws_eip.nat_eip <ip-address>
```

### Add NAT Gateway to Terraform

### Import NAT Gateway using id
```
$ terraform import aws_nat_gateway.nat nat-<id>
```

### Add Routes and Route Associating to Terraform

### Import to Terraform State
```
$ terraform import aws_route_table.public rtb-<id>
$ terraform import aws_route_table.private rtb-<id>
$ terraform import aws_route_table_association.public subnet-<id>/rtb-<id>
$ terraform import aws_route_table_association.private subnet-<id>/rtb-<id>
```

### Add Security Group and EC2 to Terraform

### Import to Terraform State
```
$ terraform import aws_security_group.nginx sg-<id>
$ terraform import aws_instance.nginx i-<id>
```

### Add Route53 Hosted Zone and A Record to Terraform

### Import to Terraform State
```
$ terraform import aws_route53_zone.devops <id>
$ terraform import aws_route53_record.nginx <id>_api.devopsbyexample.io_A
```

Once the import is successful, we will have .tf file with infrastructure configuration script.


## 2. Adding state management 

Now, that we have terraform script it is necessory to manage state globally instead of usign local drive. Since, we are talking AWS it is more relevent to us S3 as a backend for terraform.

_### Steps_

Create a `main.tf` file in a new folder (_**it should be a different folder from the project**_) like:

```
provider "aws" {
  region = "us-east-2"
}
```
Next, create an S3 bucket by using the aws_s3_bucket resource:

```
resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-up-and-running-state"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
```
Now create DynamoDB table to be used for locking :
```
resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-up-and-running-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}
```
### _Run_
```
terraform init
terraform apply
```

Next is to configure this S3 bucket with the project:

create backend.tf file(_**in the project directory**_):

```
terraform {
  backend "s3" {
    bucket         = "terraform-up-and-running-state"
    key            = "global/s3/terraform.tfstate"
    region         = "us-east-2"
    
    dynamodb_table = "terraform-up-and-running-locks"
    encrypt        = true
  }
}
```

There is more to it when we talk about different environment and we can solve this via **Isolating state files**.

## 3. Creating _jenkins_ pipeline 

Now create a pipline that goes like:

- Build the infrastructure(this step will use the S3 for state management that is above)
- Install prerequisite softwares into the instances
- Build the project 
- Test
- Deploy 

To execute terraform in jenkins pipeline:

- Install & configure Terraform plugin
- Setup basic pipeline with SCM (as required)
- Go to credentials -> global
- Create a credential of type secret text with ID aws_access_key and the access key as the secret
- Create a credential of type secret text with ID aws_secret_access_key and the access secret as the secret
  
sample Jenkinsfile
```
pipeline {
    agent any
    tools {
        "org.jenkinsci.plugins.terraform.TerraformInstallation" "terraform"
    }
    parameters {
        string(name: 'STATE_PATH', defaultValue: 'global/s3/terraform.tfstate', description: 'Path in S3 for state data')
        string(name: 'WORKSPACE', defaultValue: 'development', description:'workspace to use in Terraform')
    }

    environment {
        TF_HOME = tool('terraform')
        TF_INPUT = "0"
        TF_IN_AUTOMATION = "TRUE"
        TF_VAR_s3_address = "host.docker.internal"
        TF_LOG = "WARN"
        AWS_ACCESS_KEY_ID = credentials('aws_access_key')
        AWS_SECRET_ACCESS_KEY = credentials('aws_secret_access_key')
        PATH = "$TF_HOME:$PATH"
    }

    stages {
        stage('ApplicationInit'){
            steps {
                dir('applications/'){
                    sh 'terraform --version'
                    sh "terraform init --backend-config='path=${params.STATE_PATH}'"
                }
            }
        }
        stage('ApplicationValidate'){
            steps {
                dir('applications/'){
                    sh 'terraform validate'
                }
            }
        }
        stage('ApplicationPlan'){
            steps {
                dir('applications/'){
                    script {
                        sh "terraform plan -out terraform-applications.tfplan;echo \$? > status"
                        stash name: "terraform-applications-plan", includes: "terraform-applications.tfplan"
                    }
                }
            }
        }
        stage('ApplicationApply'){
            steps {
                script{
                    def apply = false
                    try {
                        input message: 'confirm apply', ok: 'Apply Config'
                        apply = true
                    } catch (err) {
                        apply = false
                        dir('applications/'){
                            sh "terraform destroy -auto-approve"
                        }
                        currentBuild.result = 'UNSTABLE'
                    }
                    if(apply){
                        dir('applications/'){
                            unstash "terraform-applications-plan"
                            sh 'terraform apply terraform-applications.tfplan'
                        }
                    }
                }
            }
        }
        stage('Install prerequisite software'){
            steps {
                ........................................................... to be continue.
            }
        }
    }
}
```

This pipe line will 


```
Note : This plan is just a proposed option it is incomplete and will definetly change in future will keep you updated. 
Date : 12 Aug 2021
``` 