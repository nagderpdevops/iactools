# Terraform

Terraform is an open-source infrastructure as a code software tool created by HashiCorp. It enables users to define and provision a data center infrastructure using a high-level configuration language known as Hashicorp Configuration Language (HCL), or optionally JSON.

## How to install Terraform.

### Windows

Download the .exe and add to env path.

### Mac / Linux

```
#brew install terraform
```

## Writing simple Terraform script for AWS 

Paste the configuration below into example.tf and save it. Terraform loads all files in the working directory that end in .tf.
```
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}
resource "aws_instance" "example" {
  ami           = "ami-12345678"
  instance_type = "t2.micro"
}
```
To know more about provider and resource refer to Docs.
Now,

```
$ terraform init
Initializing the backend...
Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "aws" (terraform-providers/aws) 2.10.0...
The following providers do not have any version constraints in configuration,
so the latest version was installed.
To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.
* provider.aws: version = "~> 2.10"
Terraform has been successfully initialized!
You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.
If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```
To format document run,

```
terraform fmt
```
Then validate configuration. If the configuration is valid, Terraform will return a success message.
```
terraform validate
Success! The configuration is valid.
```

## Create infrastructure

```
$ terraform apply
## ... Output truncated ...
An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create
Terraform will perform the following actions:
# aws_instance.example will be created
  + resource "aws_instance" "example" {
      + ami                          = "ami-12345678"
      + arn                          = (known after apply)
      + associate_public_ip_address  = (known after apply)
      + availability_zone            = (known after apply)
      + cpu_core_count               = (known after apply)
      + cpu_threads_per_core         = (known after apply)
      + get_password_data            = false
      + host_id                      = (known after apply)
      + id                           = (known after apply)
      + instance_state               = (known after apply)
      + instance_type                = "t2.micro"
      + ipv6_address_count           = (known after apply)
      + ipv6_addresses               = (known after apply)
## ... Output truncated ...
Plan: 1 to add, 0 to change, 0 to destroy.
```

## Inspect state

When applied configuration, Terraform wrote data into a file called terraform.tfstate. This file now contains the IDs and properties of the resources Terraform created so that it can manage or destroy those resources going forward.

We can inspect the current state using `terraform show`.

```
$ terraform show
# aws_instance.example:
resource "aws_instance" "example" {
    ami                          = "ami-12345678"
    arn                          = "arn:aws:ec2:us-east-1:130490850807:instance/i-0bbf06244e44211d1"
    associate_public_ip_address  = true
    availability_zone            = "us-east-1b"
    cpu_core_count               = 1
    cpu_threads_per_core         = 1
    disable_api_termination      = false
    ebs_optimized                = false
    get_password_data            = false
    id                           = "i-0bbf0gdhjuske789"
    instance_state               = "running"
    instance_type                = "t2.micro"
    ipv6_address_count           = 0
    ipv6_addresses               = []
    monitoring                   = false
    primary_network_interface_id = "eni-0f1ceashsji56h076"
    private_dns                  = "ip-172-31-69-121.ec2.internal"
    private_ip                   = "172.31.61.141"
    public_dns                   = "ec2-54-124-14-244.compute-1.amazonaws.com"
    public_ip                    = "54-124-14-244"
    security_groups              = [
        "default",
    ]
    source_dest_check            = true
    subnet_id                    = "subnet-1ffgj87d5"
    tenancy                      = "default"
    volume_tags                  = {}
    vpc_security_group_ids       = [
        "sg-5255f429",
    ]
credit_specification {
        cpu_credits = "standard"
    }
root_block_device {
        delete_on_termination = false
        iops                  = 100
        volume_id             = "vol-0079esdjbs567fg"
        volume_size           = 8
        volume_type           = "gp1"
    }
}
```
Now our Infrastructure is up and running. Check on AWS to see the ec2 instance details.